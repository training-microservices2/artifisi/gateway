# Menjalankan Rate Limiting

1. Jalankan redis dengan docker, expose port redis `6379` ke host
    
        docker run -p 6379:6379 redis
    
2. jalankan aplikasi gateway
    
        mvn clean spring-boot:run
    
3. Buat postman collection untuk request ke `http://localhost:9090/invoice/api/invoice/hostinfo`
   [![Postman Collection](img/2020-12-03_14-43-50.png)](img/2020-12-03_14-43-50.png)
4. Klik Run Collection, kemudian set iteration menjadi 20 dan delay 0 supaya postman menjalankan 20 request sekaligus
   [![Postman Collection](img/2020-12-03_14-45-15.png)](img/2020-12-03_14-45-15.png)
5. Amati responsenya, beberapa request awal harusnya menghasilkan response 200 OK. Request selanjutnya mendapatkan response 429 karena melewati rate limit